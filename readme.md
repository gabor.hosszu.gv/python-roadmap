# Pyhton roadmap

Szerző: [Hosszú Gábor](mailto:grindstone80p@gmail.com)

# Kezdő
Ezen a szinten legfontosabb megismerkedni az programozás és a python alapjaival és fogalmaival. Adattípusok, fügyvények, ciklusok, elágazások. Fontos, hogy értelmezni tudd a különböző hibaüzeneteket és rá tudj jól keresni a problémáidra a neten.

## Ingyenes kurzusok
Számtalan ingyen kurzus található a youtube-on, íme egy pár:
* [Python programozás kezdőknek](https://www.youtube.com/playlist?list=PLUnQJ5ZQoqQJT_sSKYFRu8vP6vJxdo3OZ)
* [Python Tutorial for Beginners - Learn Python in 5 Hours [FULL COURSE]](https://www.youtube.com/watch?v=t8pPdKYpowI)
* [Python Tutorial for Beginners - Learn Python [FULL COURSE]](https://www.youtube.com/watch?v=LzYNWme1W6Q)
* [Python Tutorial for Beginners - Learn Python in 1 Hour](https://www.youtube.com/watch?v=kqtD5dpn9C8)

## Fizetős kurzusok (udemy):
Mielőtt bármit vásárolnál az Udemy oldalán itt van pár tipp, hogyan kaphatod meg a legjobb árat:
* Indítsd az oldalt inkognitó módban
* Töröld az oldalhoz tartozó sütiket (cookie-kat)
* Telepítsd fel a [Honey](https://www.joinhoney.com/) nevű bővítményt, ami keres kuponokat a fizetés előtt

[The Python Mega Course: Build 10 Real World Applications](https://www.udemy.com/course/the-python-mega-course/)

Ez kurzus az alapoktól kezdi, de megépít 10 valódi alkalmazást is, olyan könytárakkal mint flask (web alkalmazás), foluim (térkép), pandas (adatelemézs) vagy beautifulsoup(webscapring). Nekem ez volt az első kurzusom és mindenkinek csak ajánlani tudom, mert a bemutatott projektek sokszínűek és meghozhatják a kedved egy saját projekthez.

[Python for Data Structures, Algorithms, and Interviews!](https://www.udemy.com/course/python-for-data-structures-algorithms-and-interviews/)

Szintén az alapokkal kezd, de többet foglalkozik elmélettel, úgy, mint adatstruktúrák, kereső és szortírozó algoritmusok. Emellett interjú kérdéseket és mutat, persze megoldással együtt.

## Könyvek
[Computer science distilled](https://drive.google.com/file/d/0B8_te5NGKRMQY0Z0NHJtN0IwckU/view?usp=sharing) - 
Kezdőknek szóló könyv műszaki informatikáról. Nem python specifikus, de érthető nyelven tárgylja az informatika egyik alapvető részét.

[Automate the boring stuff with python](https://drive.google.com/file/d/1YEkJSmfPNhrDRDESIJC3cZk2NCh_5wGR/view?usp=sharing) - A python irodalom egyik alapkönyve

## Projekt ötletek
* dobókocka szimulátor
* akasztófa játék
* kő, papír, olló
* kép vízjelező asztali alkalmazás
* videó letöltő szkript
* email küldő szkript
* honlap készítése

## Egyéb képességek
* mások kódjának értelmezése
* javascript
* HTML
* CSS
* SQL és adatbázisok
* Objektum orientált programozás

# Középhaladó
Ezen a szinten már egész ügyesen meg tudod valósítani az ötleteidet és megküzdesz a problémáiddal. Ideje valami nagyobba vágni a fejszét. Ismerkedj meg a beépített könyvtárakkal (random, re, collections, itertools ...) valamint komolyabb külső modulokkal (flask, django, numpy, pandas, requests, beautifulsoup, mathplotlib, openCV). Érdemes elgondolkodni valami szakosodáson (ma python nagyon népszerű a webfejlesztésben, adatelemzésben és mesterséges intelligenciában). Javaslom, hogy ezen a szinten kezd el építeni a portfóliódat egy git repo formájában.

Ezen a szinten javaslom, hogy elkezdj kódolási feladványokkal foglalkozni. Ilyen oldal az én személyes kedvencem a [Codewars](https://www.codewars.com/). Itt ne csak arra törekedj, hogy megoldd a feladatot, hanem nézd meg mások megoldásait, értelmezd őket, próbáld ki és tanulj belőlük.

## Ingyenes kurzusok
[Intermediate Python Programming Course](https://www.youtube.com/watch?v=HGOBQPFzWKo)

Egy nagyon hasznos videó, ami nem csak átismétli az alapokat, de számos haladó függvényt és könyvtárat is bemutat a hatékonyabb és szebb kódíráshoz.

## Fizetős kurzusok
Válassz valami témaspecifikusat! Az udemy, skillshare vagy más oldalon találsz bőven AI-ról, computer vision-ről, bigdata-ról vagy webfejlesztésről.

## Könyvek
Válassz valami témaspecifikusat! Minden python modulra jut 10 könyv, ha az ember nyitott szemmel jár. Érdemes a különböző közösségeket figyelni, ahol nagyon gyakran osztanak meg ilyeneket.

## Projekt ötletek
* Webscraper, ami begyűjti neked egy figyelt termék árait a netről
* Tőzsdei adatok letöltése és megjelenítése
* Sudoku megoldó
* Webkamerás arcfelismerő program
* raspberryPi projektek
* számológép alkalmazás
* memóriajáték
* valutaváló aktuális árfolyamon
* API fejlesztése

## Egyéb képességek
* Debug toolok
* Regex
* Git
* Linux
* virtuális környezet
* idő- és térkomplexitás

# Haladó
Törekedsz a jó minőségre. Az elkészített megoldásodat tovább tökélsetesíted, alaposan dokumentálod és készítes hozzá teszteket is. Vegyél rész open projektek fejlesztésében, vagy kezdj újba, ahova mások is csatlakozhatnak. Bővítsd a portfóliódat folyamatosan, üzemeld be néhány web applikációdat és tedd egy domain cím mögé. Ne csak oldj meg kódoslási feladványokat, hanem írj is, ahol törekedsz az alapos tesztelésre és érthető dokumentációra.

## Egyéb képességek
* algoritmusok összehasonlítása
* tesztelés
* dekorátorok


# Javasolt Youtube-csatornák
* [freeCodeCamp.org](https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ) - Ingyenes kurzusok és gyorstalpalok, nem csak pythonból
* [Tech With Tim](https://www.youtube.com/channel/UC4JX40jDee_tINbkjycV4Sg) - Számos python projekt és modul
* [sendtex](https://www.youtube.com/user/sentdex) - Alap és komolyabb python projektek (computer vision, AI)
* [Corey Schafer](https://www.youtube.com/user/schafer5) - Vegyes python videók, mindenkinek
* [Code Bullet](https://www.youtube.com/channel/UC0e3QhIYukixgh5VVpKHH9Q) - Gaming AI szórakoztató videók

# Egyéb források
* [Codewars](https://www.codewars.com/)
* [Magyar python programozás Facebook csoport](https://www.facebook.com/groups/python.programozok/)
* [Python Programmers Hub nemzetközi Facebook csoport](https://www.facebook.com/groups/142201439713193/)
* [Magyar nyelvű python Discord](https://discord.gg/FDruyMZAwp)
* [Grindstone80p programozási stream](https://www.twitch.tv/grindstone80p)

# Mindset
## Kíváncsiság

Próbálj ki minél több dolgot. Python-t rengeteg mindenre lehet használni a modern web-es megoldásoktól kezdve, a mesterséges intelligencián és a IoT projekteken keresztül a játékfejlesztésig.

## Kalandvágy

Azért mert egy problémára találtál egy megoldást, közel sem biztos, hogy az ideális. Tanulj meg összehasonlítani a kódrészeket egymással frappánsság, sebesség és skálázhatóság szempontjából.

## Céltudat

Ha fizetett python programozó akarsz lenni, akkor ahhoz olyan hozzáállás is kell. Tartsd szem előtt, hogy bármi, amit építesz olyan legyen, hogy büszkén felvállald a portfoliódban, amikor állást keresel.

## Minőségtudat

Működő kódot írni az alapszint. Jó minőségben dokumentálni azt egy következő szint. A fejlesztéssel párhuzamban tesztrendszert készíteni hozzá már felső kategória, amivel ki lehet tűnni az átlagból.

## Szerénység

Lehet, hogy tudsz programozni és lehet, hogy jól is. De mindig lesz, sőt kell, hogy legyen olyan, aki jobb nálad. Ne légy a [Dunning-Kruger-hatás](https://hu.wikipedia.org/wiki/Dunning%E2%80%93Kruger-hat%C3%A1s) áldozata.
